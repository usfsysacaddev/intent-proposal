	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

	<?php if(ENVIRONMENT === 'development'): ?>
	<!-- Less.js -->
	<script src="<?php echo base_url(); ?>js/less.min.js" type="text/javascript"></script>
	<?php endif; ?>

	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script>
		//DOM Object Defs
		function create_status_group()
		{
			var a = $('<div></div>').addClass('form-group').addClass('col-xs-12').css('margin-top','5px'),
				c = $('<input type="text"></input>').addClass('form-control').attr('name', 'status_text[]').attr('size', '50').attr('maxlength', '250').attr('required','required'),
				d = $('<button class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></button>').click(function(e) {
					e.preventDefault();
					$(this).closest('.form-group.col-xs-12').remove();
				});

			a.append(c)
				.append(d);

			return a;
		}

		function create_comment_group()
		{
			var a = $('<div></div>').addClass('form-group').addClass('col-xs-12').css('margin-top','5px'),
				c = $('<input type="text"></input>').addClass('form-control').attr('name', 'comment_text[]').attr('size', '50').attr('maxlength', '250').attr('required','required'),
				d = $('<button class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></button>').click(function(e) {
					e.preventDefault();
					$(this).closest('.form-group.col-xs-12').remove();
				});

			a.append(c)
				.append(d);

			return a;
		}

		function create_file_group()
		{
			var a = $('<div></div>').addClass('form-group').addClass('col-xs-12').css('margin-top','5px'),
				b = $('<input type="file"></input>').addClass('form-control').attr('name', 'files[]').attr('accept','.pdf,.doc,.docx'),
				c = $('<input type="text"></input>').addClass('form-control').attr('name', 'doc_name[]').attr('size', '40').attr('maxlength', '100').attr('placeholder','Display Name').attr('required','required'),
				d = $('<button class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></button>').click(function(e) {
					e.preventDefault();
					$(this).closest('.form-group.col-xs-12').remove();
				});

			a.append(b)
				.append('&nbsp;')
				.append(c)
				.append('&nbsp;')
 				.append(d);

 			return a;
 		}
		$(function() {
			var statusGroup = $('.status-group'),
					commentGroup = $('.comment-group'),
				fileGroup = $('.file-group');

			$('.add-status').click(function(e) {
				e.preventDefault();
				statusGroup.append(create_status_group());
			});
			$('.add-comment').click(function(e) {
				e.preventDefault();
				commentGroup.append(create_comment_group());
			});
			$('.add-file').click(function(e) {
				e.preventDefault();
				fileGroup.append(create_file_group());
			});
		});
		$(function() {
			$('.del-int-prop').click(function(e) {
				e.preventDefault();
				var row = $(this).closest('tr');
				//Do AJAX call
				if(window.XMLHttpRequest) { xmlhttp = new XMLHttpRequest(); } else { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						row.fadeOut('500');
					}
				}
				xmlhttp.open('POST', '<?php echo base_url(); ?>intent-proposal/do_delete', true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send('prop_id=' + $(this).data('propid'));
			});
		});
	</script>
</body>
</html>
