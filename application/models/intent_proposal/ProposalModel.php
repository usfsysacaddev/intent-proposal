<?php
    /**
	 *
	 * New Intent proposal
	 *
	 * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */

    require_once('DocumentFactory.php');
    require_once('StatusFactory.php');
    require_once('CommentFactory.php');
    require_once('DocumentModel.php');
    require_once('StatusModel.php');
    require_once('CommentModel.php');
    require_once(APPPATH . 'helpers/utils_helper.php');

	class ProposalModel extends CI_Model
	{

		/**
	     * Origin
	     *
	     * @var string $origin
	     * @access public
	     */
		public $origin;

		/**
	     * Proposal ID
	     *
	     * @var string $prop_id
	     * @access public
	     */
		public $prop_id;

		/**
	     * Campus
	     *
	     * @var string $_campus
	     * @access protected
	     */
		protected $_campus;

		/**
	     * College Code
	     *
	     * @var string $_coll_code
	     * @access protected
	     */
		protected $_coll_code;

    /**
       * Type
       *
       * @var string type
       * @access public
       */
    public $type;

		/**
	     * Degree Code
	     *
	     * @var string $degc_code
	     * @access public
	     */

		public $degc_code;

		/**
	     * Proposal Title
	     *
	     * @var string $title
	     * @access public
	     */
		public $title;

		/**
	     * CIP Code
	     *
	     * @var string $_cipc_code
	     * @access protected
	     */
		protected $_cipc_code;

    /**
      * 14-Day Review Period
      *
      * @var string $review_period
      * @access public
      */

    public $review_period;

    /**
      * Concerns
      *
      * @var string $concerns
      * @access public
      */
    public $concerns;

    /**
	     * CIP Code Track
	     *
	     * @var string $_cipc_track
	     * @access protected
	     */
		protected $_cipc_track;

		/**
	     * Faculty Proposer
	     *
	     * @var string $faculty_proposer
	     * @access public
	     */
		public $faculty_proposer;

		/**
	     * Archive flag
	     *
	     * @var int $archive_flag
	     * @access public
	     */
		public $archive_flag;

		/**
	     * Collection of documents
	     * @var array $document_collection array of document objects
	     * @access public
	     */
		public $document_collection;

		/**
	     * Collection of statuses
	     *
	     * @var array $status_collection array of status objects
	     * @access public
	     */
		public $status_collection;

    /**
	     * Collection of comments
	     *
	     * @var array $comment_collection array of comment objects
	     * @access public
	     */
		public $comment_collection;

		/**
		 * Creates document object.
		 *
		 * @param array $params OPTIONAL document parameters
		 * @access public
		 */
		public function __construct($params = array())
		{
			$this->origin = 'new';
			$this->set_params($params);
		}

		/**
		 * Sets proposal parameters.
		 *
		 * Protected until admin
		 * @param array $params parameters to be set
		 * @access protected
		 */
		protected function set_params($params)
		{
			$params = (array)$params;
			$this->prop_id = isset($params['prop_id']) ? get_binary($params['prop_id']) : $this->prop_id;
			$this->degc_code = isset($params['degc_code']) ? $params['degc_code'] : $this->degc_code;
      $this->type = isset($params['type']) ? $params['type'] : $this->type;
			$this->title = isset($params['title']) ? $params['title'] : $this->title;
      $this->review_period = isset($params['review_period']) ? $params['review_period'] : $this->review_period;
      $this->concerns = isset($params['concerns']) ? $params['concerns'] : $this->concerns;
			$this->faculty_proposer = isset($params['faculty_proposer']) ? $params['faculty_proposer'] : $this->faculty_proposer;
			$this->archive_flag = isset($params['archive_flag']) ? $params['archive_flag'] : $this->archive_flag;

			!isset($params['campus']) ?: $this->set_campus($params['campus']);
			!isset($params['coll_code']) ?: $this->set_coll_code($params['coll_code']);
			!isset($params['cipc_code']) ?: $this->set_cipc_code($params['cipc_code']);
			!isset($params['cipc_track']) ?: $this->set_cipc_track($params['cipc_track']);

			//Handle CIP Code Track splitting (user interface splits into two) FIXME: Should probably store this in db as two fields?
			if(isset($params['cipc_track1']) && isset($params['cipc_track2']))
				$this->set_cipc_track(trim($params['cipc_track1']) . ' of ' . trim($params['cipc_track2']));

			//Handle Documents (passed as array of ids) FIXME: Any way to optimize this? Making multiple DB calls otherwise
			if(isset($params['documents']))
			{
				$this->document_collection = null;
				foreach($params['documents'] as $doc_id)
				{
					//Create document
					$doc = DocumentFactory::create_document();
					$doc->set_params_by_id($doc_id); //existing document

					//Add to collection
					$this->add_to_document_collection($doc);
				}
			}

			//Handle statuses (passed as array of status ids)
			if(isset($params['statuses']))
			{
				$this->status_collection = null;
				foreach($params['statuses'] as $status_id)
				{
					//Create status
					$status = StatusFactory::create_status();
					$status->set_params_by_id($status_id); //existing status

					//Add to collection
					$this->add_to_status_collection($status);
				}
			}

      //Handle comments (passed as array of comment ids)
      if(isset($params['comments']))
      {
        $this->comment_collection = null;
        foreach($params['comments'] as $comment_id)
        {
          //Create comment
          $comment = CommentFactory::create_comment();
          $comment->set_params_by_id($comment_id); //existing comment

          //Add to collection
          $this->add_to_comment_collection($comment);
        }
      }
		}

		/**
		 * Sets proposal parameters by proposal id.
		 *
		 * @param string $prop_id proposal id to fetch from DB
		 * @return bool success flag
		 * @access public
		 */
		public function set_params_by_id($prop_id)
		{
			//Attempt to fetch from db
      $this->load->database();
			$this->db->select('intent_proposals.prop_id, campus, coll_code, type, degc_code,
                         title, cipc_code, review_period, concerns, cipc_track, faculty_proposer, archive_flag,
                         GROUP_CONCAT(DISTINCT HEX(doc_id) ORDER BY intent_prop_documents.order SEPARATOR "|") AS documents,
                         GROUP_CONCAT(DISTINCT HEX(status_id) ORDER BY intent_prop_status.order SEPARATOR "|") AS statuses,
                         GROUP_CONCAT(DISTINCT HEX(comment_id) ORDER BY comment_text SEPARATOR "|") AS comments', false);
			$this->db->from('intent_proposals');
			$this->db->join('intent_prop_documents', 'intent_proposals.prop_id = intent_prop_documents.prop_id', 'left');
			$this->db->join('intent_prop_status', 'intent_proposals.prop_id = intent_prop_status.prop_id', 'left');
      $this->db->join('intent_prop_comments', 'intent_proposals.prop_id = intent_prop_comments.prop_id', 'left');
			$this->db->group_by('prop_id');
			$this->db->where('intent_proposals.prop_id', get_binary($prop_id));
			$query = $this->db->get();

			//if result, set object params
			$success = false;
			if($query->row())
			{
				$data = $query->row();
				$data->documents = empty($data->documents) ? null : explode('|', $data->documents);
				$data->statuses = empty($data->statuses) ? null : explode('|', $data->statuses);
        $data->comments = empty($data->comments) ? null : explode('|', $data->comments);

				$this->origin = 'existing';
				$this->set_params((array)$query->row());
				$success = true;
			}

			return $success;
		}

		/**
		 * Sets Institution Code
		 *
		 * @param string $campus Institution Code. Must be USF, USFSP, USFSM, USFH, or SYS (ALL)
		 * @access public
		 */
		public function set_campus($campus)
		{
			$campus = strtoupper($campus);
			if($campus == 'T' || $campus == 'P' || $campus == 'S' || $campus == 'T, P' || $campus == "T, S" || $campus == "P, S" || $campus == "T, P, S" || $campus == 'SYS')
				$this->_campus = $campus;
		}

		/**
		 * Sets college code
		 *
		 * @param string $coll_code College Code. Must be uppercase, max 2 characters
		 * @access public
		 */
		public function set_coll_code($coll_code)
		{
			$coll_code = strtoupper($coll_code);
			if(strlen($coll_code) === 2)
				$this->_coll_code = $coll_code;
		}

		/**
		 * Sets CIP Code
		 *
		 * @param string $cipc_code Must be 6 digits formatted with period (##.####)
		 * @access public
		 */
		public function set_cipc_code($cipc_code)
		{
			if(strlen($cipc_code) === 7 && substr($cipc_code, 2, 1) == '.')
				$this->_cipc_code = $cipc_code;
		}

		/**
		 * Sets CIP Code Track
		 *
		 * @param string $cipc_track Should be formatted as ## of ##
		 * @access public
		 */
		public function set_cipc_track($cipc_track)
		{
			if(preg_match("/^[0-9][0-9]? of [0-9][0-9]?$/", $cipc_track))
				$this->_cipc_track = $cipc_track;
		}

		/**
		 * Gets inst code
		 *
		 * @access public
		 */
		public function get_campus()
		{
			return $this->_campus;
		}

		/**
		 * Gets coll code
		 *
		 * @access public
		 */
		public function get_coll_code()
		{
			return $this->_coll_code;
		}

		/**
		 * Gets cipc code
		 *
		 * @access public
		 */
		public function get_cipc_code()
		{
			return $this->_cipc_code;
		}

		/**
		 * Gets cipc track
		 *
		 * @access public
		 */
		public function get_cipc_track()
		{
			return $this->_cipc_track;
		}

		/**
		 * Adds Document to collection
		 *
		 * @param object $document the DocumentModel object to be added to the collection
		 * @access public
		 */
		public function add_to_document_collection($document)
		{
			//Document must have ID before going in
			if(method_exists($document, 'create_id'))
				$document->create_id();

			//Add to collection
			$this->document_collection[bin2hex($document->doc_id)] = $document;
		}

		/**
		 * Adds Status to collection
		 *
		 * @param object $status the StatusModel object to be added
		 * @access public
		 */
		public function add_to_status_collection($status)
		{
			//Status must have ID before going in.
			if(method_exists($status, 'create_id'))
				$status->create_id();

			//Add to collection
			$this->status_collection[bin2hex($status->status_id)] = $status;
		}

    /**
     * Adds Comment to collection
     *
     * @param object $comment the CommentModel object to be added
     * @access public
     */
    public function add_to_comment_collection($comment)
    {
      //Comment must have ID before going in.
      if(method_exists($comment, 'create_id'))
        $comment->create_id();

      //Add to collection
      $this->comment_collection[bin2hex($comment->comment_id)] = $comment;
    }
	}
?>
