<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Intent_Proposal extends CI_Controller {
	/**
	 * Handle constructor
	 */
	 public function __construct()
 	{
 		parent::__construct();
		require_once('D:\IIS\academicplanning$\wwwroot\guard.php');

		//Entire section is off limits to anyone not authorized
		//Check that user is authorized to be here
		$this->load->model('UserModel');
		if(!$this->UserModel->is_authorized)
		{
			show_error('User not authenticated.', 403);
			log_message('error', 'User authentication failed: ' . $this->UserModel->username);
			die();
		}
 	}

	/**
	 * Add new proposal form
	 */
	public function add()
	{
		$this->load->model('intent_proposal/AdminProposalModel', 'AdminProposalModel');
		$this->load->helper(array('output','utils'));
		$this->load->view('intent_proposal/admin_proposal', array('proposal' => $this->AdminProposalModel, 'user' => $this->UserModel));
	}

	/**
	 * Edit proposal form
	 */
	public function edit()
	{
		if($this->input->get('prop_id'))
		{
			$this->load->model('intent_proposal/AdminProposalModel', 'AdminProposalModel');
			$this->AdminProposalModel->set_params_by_id($this->input->get('prop_id'));

			if($this->UserModel->can_edit($this->AdminProposalModel->get_campus()))
			{
				$this->load->helper(array('output','utils'));
				$this->load->view('intent_proposal/admin_proposal', array('proposal' => $this->AdminProposalModel, 'user' => $this->UserModel));
			}
			else
			{
				show_error('User not authenticated.', 403);
				log_message('error', 'User authentication failed: ' . $this->UserModel->username);
				die();
			}
		}
	}

	/**
	 * Do program save
	 */
	public function do_save()
	{

		//If user can edit...
		if($this->UserModel->can_edit($this->input->post('campus')))
		{
			//Create proposal
			$this->load->model('intent_proposal/AdminProposalModel', 'AdminProposalModel');

			//Check for existing (editing & saving)
			if($this->input->post('prop_id'))
			{
				//Populate existing
				$this->AdminProposalModel->set_params_by_id($this->input->post('prop_id'));
			}

			if(is_array($this->input->post('document_order')))
			{
				foreach($this->input->post('document_order') as $doc_id => $order)
				{
					if($order > 0)
					{
						$this->AdminProposalModel->document_collection[$doc_id]->order=$order;
					}
				}
			}

			//Perform any status, comment, and document deletes
			if($this->input->post('document_del'))
			{
				foreach($this->input->post('document_del') as $doc_id)
				{
					$this->AdminProposalModel->document_collection[$doc_id]->delete();
					$this->AdminProposalModel->remove_from_document_collection($doc_id);
				}
			}

		if(is_array($this->input->post('status_order')))
		{
			foreach($this->input->post('status_order') as $status_id => $order)
			{
				if($order > 0)
				{
					$this->AdminProposalModel->status_collection[$status_id]->order=$order;
				}
			}
		}

			if($this->input->post('status_del'))
			{
				foreach($this->input->post('status_del') as $status_id)
				{
					$this->AdminProposalModel->status_collection[$status_id]->delete();
					$this->AdminProposalModel->remove_from_status_collection($status_id);
				}
			}

			if($this->input->post('comment_del'))
			{
				foreach($this->input->post('comment_del') as $comment_id)
				{
					$this->AdminProposalModel->comment_collection[$comment_id]->delete();
					$this->AdminProposalModel->remove_from_comment_collection($comment_id);
				}
			}

			//Update proposal
			$this->AdminProposalModel->set_params($this->input->post());
			$this->AdminProposalModel->create_id(); //just in case

			//Create Statuses
			//Adds to Status Column in Table and saves entries on Admin Panel
			for($i = 0; $i < sizeof((array)$this->input->post('status_text')); $i += 1)
			{
				if($this->input->post('status_text')[$i])
					$this->AdminProposalModel->add_to_status_collection(StatusFactory::create_status(array(
						'prop_id' => $this->AdminProposalModel->prop_id,
						'status_text' => $this->input->post('status_text')[$i],
						'status_date' => isset($this->input->post('status_date')[$i]) ? $this->input->post('status_date')[$i] : NULL)));
			}

			//Create Comments
			for($i = 0; $i < sizeof((array)$this->input->post('comment_text')); $i += 1)
			{
				if($this->input->post('comment_text')[$i])
					$this->AdminProposalModel->add_to_comment_collection(CommentFactory::create_comment(array(
						'prop_id' => $this->AdminProposalModel->prop_id,
						'comment_text' => $this->input->post('comment_text')[$i],
						'comment_date' => isset($this->input->post('comment_date')[$i]) ? $this->input->post('comment_date')[$i] : NULL)));
			}

			//Upload Documents
			if(sizeof((array)$this->input->post('doc_name')) > 0)
			{
				$config = array(
					'upload_path' => doc_path(),
					'allowed_types' => 'pdf|doc|docx'
					);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				for($i = 0; $i < sizeof((array)$this->input->post('doc_name')); $i += 1)
				{
					//CodeIgniter doesn't work with file arrays, so create tmp_file for each
					$_FILES['tmp_file'] = array(
						'name' => isset($_FILES['files']['name'][$i]) ? strtolower($this->input->post('campus') . '_' . $_FILES['files']['name'][$i]) : null,
						'type' => isset($_FILES['files']['type'][$i]) ? $_FILES['files']['type'][$i] : null,
						'tmp_name' => isset($_FILES['files']['tmp_name'][$i]) ? $_FILES['files']['tmp_name'][$i] : null,
						'error' => isset($_FILES['files']['error'][$i]) ? $_FILES['files']['error'][$i] : null,
						'size' => isset($_FILES['files']['size'][$i]) ? $_FILES['files']['size'][$i] : null
						);

					//Attempt CodeIgniter upload
					if(!$this->upload->do_upload('tmp_file'))
					{
						show_error('Upload failed.' . $this->upload->display_errors());
						log_message('error', 'Upload failure:' . $this->upload->display_errors());
						$i = 999;
					}
					//Create document object if successful
					else
					{
						$this->AdminProposalModel->add_to_document_collection(DocumentFactory::create_document(array(
							'prop_id' => $this->AdminProposalModel->prop_id,
							'doc_name' => $this->input->post('doc_name')[$i],
							'filename' => $this->upload->data()['file_name']
						)));
					}
				}
			}

			//Save
			$this->AdminProposalModel->save();

			//Send to listing page
			redirect(base_url() . 'intent-proposal-collection/admin');
		}
	  else
		{
			show_error('User not authenticated.', 403);
			log_message('error', 'User authentication failed: ' . $this->UserModel->username);
			die();
		}
	}

	/**
	 * Do program delete
	 */
	public function do_delete()
	{
		$success = false;

		if($this->input->post('prop_id'))
		{
			$this->load->model('intent_proposal/AdminProposalModel', 'AdminProposalModel');
			$this->AdminProposalModel->set_params_by_id($this->input->post('prop_id'));

			if($this->UserModel->can_delete($this->AdminProposalModel->get_campus()))
				$success = $this->AdminProposalModel->delete();
		}

		if($success)
			header('HTTP/1.0 200 OK', true, 200);
		else
			header('HTTP/1.0 400 Bad Request', true, 400);
	}
}
