<!-- Adding/Editing Intent Proposal Web Page -->
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('_adminheader');
?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2>Intent Proposal</h2>
			</div>
		</div>
		<div class="row" style="margin-top:20px;">
			<form class="form-inline" method="post" action="<?php echo base_url(); ?>intent_proposal/do_save" enctype="multipart/form-data">
				<input type="hidden" name="prop_id" value="<?php echo bin2hex($proposal->prop_id); ?>">
				<div class="col-xs-12">
					<div class="form-group">
						<label for="campus">Campus</label>
							<select class="form-control" id="campus" name="campus" title="T is for Tampa, P is for St. Pete, S is for Sarasota" required>
								<option></option>
								<option<?php if($proposal->get_campus()=='T'): ?> selected<?php endif; ?>>T</option>
								<option<?php if($proposal->get_campus()=='P'): ?> selected<?php endif; ?>>P</option>
								<option<?php if($proposal->get_campus()=='S'): ?> selected<?php endif; ?>>S</option>
								<option<?php if($proposal->get_campus()=='T, P'): ?> selected<?php endif; ?>>T, P</option>
								<option<?php if($proposal->get_campus()=='T, S'): ?> selected<?php endif; ?>>T, S</option>
								<option<?php if($proposal->get_campus()=='P, S'): ?> selected<?php endif; ?>>P, S</option>
								<option<?php if($proposal->get_campus()=='T, P, S'): ?> selected<?php endif; ?>>T, P, S</option>
							</select>
					</div>
					&nbsp;&nbsp;
					<div class="form-group">
						<label for="coll_code">College Code<small> (i.e., AC)</small></label>
						<input type="text" class="form-control" id="coll_code" name="coll_code" size="2" maxlength="2" value="<?php echo $proposal->get_coll_code(); ?>" required>
					</div>
					&nbsp;&nbsp;
					<div class="form-group">
						<label for="type">Type</label>
						<input type="text" class="form-control" id="type" name="type" size="5" maxlength="50" value="<?php echo $proposal->type; ?>" required>
					</div>
					&nbsp;&nbsp;
					<div class="form-group">
						<label for="degc_code">Degree Code<small> (i.e., M.S.)</small></label>
						<input type="text" class="form-control" id="degc_code" name="degc_code" size="5" maxlength="12" value="<?php echo $proposal->degc_code; ?>" required>
					</div>
				</div>
				<div class="col-xs-12" style="margin-top:15px;">
					<div class="form-group">
						<label for="cipc_code">CIP Code <small>(formatted ##.####)</small></label>
						<input type="text" class="form-control" id="cipc_code" name="cipc_code" size="7" pattern="[0-9][0-9].[0-9][0-9][0-9][0-9]" value="<?php echo $proposal->get_cipc_code(); ?>" required>
					</div>
					&nbsp;&nbsp;
					<div class="form-group">
						<label for="cipc_track1">CIP Track (Optional) <small>(formatted ## of ##)</small></label>
						<input type="text" class="form-control" id="cipc_track1" name="cipc_track1" size="1" pattern="[0-9]?[0-9]" value="<?php echo get_cipc_track_part($proposal->get_cipc_track(),1); ?>">
						of
						<input type="text" class="form-control" id="cipc_track2" name="cipc_track2" size="1" pattern="[0-9]?[0-9]" value="<?php echo get_cipc_track_part($proposal->get_cipc_track(),2); ?>">
					</div>
				</div>
				<div class="col-xs-12" style="margin-top:15px;">
					<div class="form-group">
						<label for="title">Program Title</label>
						<input type="text" class="form-control" id="title" name="title" size="60" maxlength="250" value="<?php echo $proposal->title; ?>" required>
					</div>
				</div>
				<div class="col-xs-12" style="margin-top:15px;">
					<div class="form-group">
						<label for="title">14 Day Review Period</label>
						<input type="text" class="form-control" id="review_period" name="review_period" size="60" maxlength="250" value="<?php echo $proposal->review_period; ?>" required>
					</div>
				</div>
				<div class="col-xs-12" style="margin-top:15px;">
					<div class="form-group">
						<label for="concerns">Concerns?</label>
						<select class="form-control" id="concerns" name="concerns">
							<option> </option>
							<option value="Yes"<?php if(!$proposal->concerns):?> selected<?php endif; ?>>Yes</option>
							<option value="No"<?php if($proposal->concerns):?> selected<?php endif; ?>>No</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12" style="margin-top:15px;">
					<div class="form-group">
						<label for="faculty_proposer">Faculty Proposer</label>
						<input type="text" class="form-control" id="faculty_proposer" name="faculty_proposer" size="60" maxlength="200" value="<?php echo $proposal->faculty_proposer; ?>" required>
					</div>
				</div>
				<div class="col-xs-12" style="margin-top:15px;">
					<div class="form-group">
						<label for="archive_flag">Archive</label>
						<select class="form-control" id="archive_flag" name="archive_flag" required>
							<option value="0"<?php if(!$proposal->archive_flag):?> selected<?php endif; ?>>No</option>
							<option value="1"<?php if($proposal->archive_flag):?> selected<?php endif; ?>>Yes</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12" style="margin-top:25px;border-top:1px #ccc solid;">
					<h3>Status(es)</h3>
					<p><button class="btn btn-primary add-status">Add <span class="glyphicon glyphicon-plus"></span></button></p>
					<!--<div class="alert alert-info col-xs-12 col-md-8" role="alert">
						<span class="glyphicon glyphicon-info-sign"></span> Statuses will automatically appear in alphabetical order.
					</div>-->
						<div class="col-xs-12 col-md-6">
							<table class="table table-striped">
								<thead><tr><th>Existing Status</th><th>Order</th><th>Last Modified</th><th>Modified By</th><th>Delete?</th></tr></thead>
								<tbody>
									<?php if (is_array($proposal->status_collection) || is_object($proposal->status_collection)):?>
									<?php foreach($proposal->status_collection as $status): ?>
										<tr><td><?php echo $status->status_text; ?></td>
											<td><input type="number" name ="status_order[<?php echo bin2hex($status->status_id); ?>]" class="form-control" style="max-width:100px;" value="<?php echo $status->order; ?>"></td>
											<td><?php if($status->modified_status_date) { echo date("m/d/y h:i a",strtotime($status->modified_status_date)); } ?></td><td>
											<?php echo $status->modified_status_user; ?></td>
											<td><input type="checkbox" name="status_del[]" value="<?php echo bin2hex($status->status_id); ?>"></td></tr>
									<?php endforeach; ?>
								<?php endif; ?>
								</tbody>
							</table>
						</div>
					<div class="status-group">
						<div class="form-group col-xs-12" style="margin-top: 5px;">
							<input type="text" class="form-control" name="status_text[]" size="50" maxlength="250" placeholder="Status Text"<?php if(!$proposal->status_collection): ?><?php endif; ?>>
						</div>
					</div>
				</div>
				<div class="col-xs-12" style="margin-top:25px;border-top:1px #ccc solid;">
					<h3>Comment(s)</h3>
					<p><button class="btn btn-primary add-comment">Add <span class="glyphicon glyphicon-plus"></span></button></p>
					<!--<div class="alert alert-info col-xs-12 col-md-8" role="alert">
						<span class="glyphicon glyphicon-info-sign"></span> Comments will automatically appear in alphabetical order.
					</div>-->
						<div class="col-xs-12 col-md-6">
							<table class="table table-striped">
								<thead><tr><th>Existing Comment</th><th>Last Modified</th><th>Modified By</th><th>Delete?</th></tr></thead>
								<tbody>
									<?php if (is_array($proposal->comment_collection) || is_object($proposal->comment_collection)):?>
									<?php foreach($proposal->comment_collection as $comment): ?>
										<tr><td><?php echo $comment->comment_text; ?></td><td><?php if($comment->modified_comment_date) { echo date("m/d/y h:i a",strtotime($comment->modified_comment_date)); } ?></td><td>
											<?php echo $comment->modified_comment_user; ?></td><td><input type="checkbox" name="comment_del[]" value="<?php echo bin2hex($comment->comment_id); ?>"></td></tr>
									<?php endforeach; ?>
								<?php endif; ?>
								</tbody>
							</table>
						</div>
					<div class="comment-group">
						<div class="form-group col-xs-12" style="margin-top: 5px;">
							<input type="text" class="form-control" name="comment_text[]" size="50" maxlength="250" placeholder="Comment Text"<?php if(!$proposal->comment_collection): ?><?php endif; ?>>
						</div>
					</div>
				</div>
					<div class="col-xs-12" style="margin-top:25px;border-top:1px #ccc solid;">
						<h3>File Upload</h3>
						<p><button class="btn btn-primary add-file">Add <span class="glyphicon glyphicon-plus"></span></button></p>
						<!--<div class="alert alert-info col-xs-12 col-md-8" role="alert">
							<span class="glyphicon glyphicon-info-sign"></span> Files will automatically appear in alphabetical order.
						</div>-->
							<div class="col-xs-12 col-md-6">
								<table class="table table-striped">
									<thead><tr><th>Existing Document</th><th>Order</th><th>Last Modified</th><th>Modified By</th><th>Delete?</th></tr></thead>
									<tbody>
										<?php if (is_array($proposal->document_collection) || is_object($proposal->document_collection)):?>
										<?php foreach($proposal->document_collection as $document): ?>
											<tr><td><a href="<?php echo doc_url() . $document->filename; ?>" target="_blank"><?php echo $document->doc_name; ?></a></td>
												<td><input type="number" name ="document_order[<?php echo bin2hex($document->doc_id); ?>]" class="form-control" style="max-width:100px;" value="<?php echo $document->order; ?>"></td>
												<td><?php if($document->modified_doc_date) { echo date("m/d/y h:i a",strtotime($document->modified_doc_date)); } ?></td>
													<td><?php echo $document->modified_doc_user; ?></td>
													<td><input type="checkbox" name="document_del[]" value="<?php echo bin2hex($document->doc_id); ?>"></td></tr>
										<?php endforeach; ?>
									<?php endif; ?>
									</tbody>
								</table>
							</div>
						<div class="file-group"></div>
					</div>
				<!--	<div class="col-xs-12" style="margin-top:25px;border-top:1px #ccc solid;">-->
				<div class="col-xs-12" style="margin-top:25px;padding-top:15px;border-top:1px #ccc solid;">
					<p class="text-center"><input type="submit" value="Save" class="btn btn-lg btn-primary" style="padding-left:2em;padding-right:2em;"></p>
				</div>
			</form>
		</div>
	<!--</div>-->
<?php $this->load->view('_adminfooter'); ?>
