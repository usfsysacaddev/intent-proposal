<?php
    /**
	 * StatusModel (Admin)
	 *
	 * Status model admin extension.
	 *
   * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */

    require_once('StatusModel.php');
	class AdminStatusModel extends StatusModel
	{
		/**
		 * Handles user authentication, if not already done.
		 * Creates status object.
		 *
		 * @param array $params the parameters for this status object
		 * @access public
		 */
		public function __construct($params = array())
		{
			parent::__construct();

			//Check if the user has already been created
			if(!class_exists("UserModel"))
			{
				//Attempt to authenticate user
				$framework = get_instance();
				$framework->load->model('UserModel');
				if(!$this->UserModel->is_authorized)
				{
					//Log error and kill
					show_error('User not authenticated.', 403);
					log_message('error', 'User authentication failed: ' . $this->UserModel->username);
					die();
				}
			}

			$this->set_params($params);
		}

		/**
		 * Sets status parameters.
		 *
		 * @param array $params parameters to be set
		 * @access public
		 */
		public function set_params($params)
		{
			parent::set_params($params); //we're just loosening visibility
		}

		/**
		 * Saves status data to db
		 *
		 * @access public
		 */
		public function save()
		{
			//Create status id, if needed
			$this->create_id();

			//Create data array
			$data = array(
				'status_id' => $this->status_id,
				'prop_id' => $this->prop_id,
				'status_text' => $this->status_text,
        'order' => $this->order,
				'modified_status_date' => date('Y-m-d G:i:s'),
				'modified_status_user' => $this->UserModel->username
				);

			//Commit to db
			$this->db->replace('intent_prop_status', $data);
		}

		/**
		 * Deletes status from db
		 *
		 * @return bool success flag
		 * @access public
		 */
		public function delete()
		{
			//Remove database record
			$success = $this->db->delete('intent_prop_status', array('status_id' => $this->status_id)); 

			return $success;
		}

		/**
		 * Create Status ID
		 *
		 * @access public
		 */
		public function create_id()
		{
			$this->status_id = empty($this->status_id) ? hex2bin(md5($this->prop_id . $this->status_text)) : $this->status_id;
		}
	}
?>
