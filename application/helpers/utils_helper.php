<?php
	function get_binary($val)
	{
		return ctype_xdigit($val) ? hex2bin($val) : $val;
	}
	function doc_url()
	{
		return 'https://academicplanning.usf.edu/intent-proposal/docs/';
	}
	function doc_path()
	{
		return '/IIS/academicplanning$/wwwroot/intent-proposal/docs/';
	}
