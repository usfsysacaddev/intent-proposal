<?php
    /**
	 * Comment Factory
	 *
	 * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */

	class CommentFactory
	{
		/**
		 * Creates comment object.
		 *
		 * @param array $params OPTIONAL comment parameters
		 * @return object comment object, false if none
		 * @access public
		 * @static
		 */
		public static function create_comment($params = array())
		{
			$obj = false;
			if(class_exists('AdminCommentModel'))
				$obj = new AdminCommentModel($params);
			elseif(class_exists('CommentModel'))
				$obj = new CommentModel($params);

			return $obj;
		}
	}
?>
