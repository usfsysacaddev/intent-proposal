<!-- New Intent Proposal Admin Tracking -->
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('_adminheader');
?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2>New Intent Proposals</h2>
				<p><a href="<?php echo base_url(); ?>intent-proposal/add" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-plus-sign"></span> Add New</a></p>
			</div>
		</div>
		<div class="row" style="margin-top:20px;">
			<div class="col-xs-12">
				<table class="table table-striped">
					<thead>
						<tr>
								<th style="text-align:center">College</th>
								<th style="text-align:center">Campus</th>
								<th style="text-align:center">Degree Code</th>
								<th style="text-align:center">Type</th>
								<th style="text-align:center">Title</th>
								<th style="text-align:center">CIP Code</th>
								<th style="text-align:center">Faculty Proposer</th>
								<th style="text-align:center">Review Period</th>
								<th style="text-align:center">Concerns?</th>
								<th style="text-align:center">Status</th>
								<th style="text-align:center">Documents</th>
								<th style="text-align:center">Comments</th>
							</tr>
					</thead>
					<tbody>
						<?php if($proposals): ?>
							<?php foreach($proposals as $proposal): ?>
								<?php if(!$proposal->archive_flag): ?>
								<tr>
									<td style="text-align:center"><?php echo $proposal->get_coll_code(); ?></td>
									<td style="text-align:center"><?php echo $proposal->get_campus(); ?></td>
									<td style="text-align:center"><?php echo $proposal->degc_code; ?></td>
									<td style="text-align:center"><?php echo $proposal->type; ?></td>
									<td style="text-align:center"><?php echo $proposal->title; ?></td>
									<td style="text-align:center"><?php echo format_cip($proposal->get_cipc_code(), $proposal->get_cipc_track()); ?></td>
									<td style="text-align:center"><?php echo $proposal->faculty_proposer; ?></td>
									<td style="text-align:center"><?php echo $proposal->review_period; ?></td>
									<td style="text-align:center"><?php echo $proposal->concerns; ?></td>
									<td><ul style="margin-bottom:0;">
									</ul><?php if(!empty($proposal->status_collection)): ?>
											<ul style="margin-bottom:0;">
											<?php foreach($proposal->status_collection as $status): ?>
												<li><?php echo $status->status_text; ?></li>
											<?php endforeach; ?>
										</ul>
										<?php endif; ?></td>
									<td><ul style="margin-bottom:0;">
									</ul><?php if(!empty($proposal->document_collection)): ?>
											<ul style="margin-bottom:0;">
											<?php foreach($proposal->document_collection as $document): ?>
												<li><a href="<?php echo doc_url() . $document->filename; ?>" target="_blank"><?php echo $document->doc_name; ?></a></li>
											<?php endforeach; ?>
											</ul>
										<?php endif; ?></td>
										<td><ul style="margin-bottom:0;">
									</ul><?php if(!empty($proposal->comment_collection)): ?>
										<ul style="margin-bottom:0;">
												<?php foreach($proposal->comment_collection as $comment): ?>
													<li><?php echo $comment->comment_text; ?></li>
												<?php endforeach; ?>
												</ul>
											<?php endif; ?></td>
									<td style="min-width:100px;"><a href="<?php echo base_url(); ?>intent-proposal/edit?prop_id=<?php echo bin2hex($proposal->prop_id); ?>" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
										<?php if($this->UserModel->can_delete($proposal->get_campus())): ?>
											<button class="btn btn-danger del-int-prop" data-propid="<?php echo bin2hex($proposal->prop_id); ?>"><span class="glyphicon glyphicon-trash"></span></button>
										<?php endif; ?></td>
								</tr>
								<?php endif; ?>
							<?php endforeach; ?>
						<?php else: ?>
							<tr><td colspan="12" class="text-center">No proposals available.</td></tr>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>

	<!-- Archive Intent Proposal Admin Tracking -->
		<div class="row" style="margin-top:20px;">
			<div class="col-xs-12">
				<h2>Archive Intent Proposals</h2>
				<table class="table table-striped">
					<thead>
						<tr>
								<th style="text-align:center">College</th>
								<th style="text-align:center">Campus</th>
								<th style="text-align:center">Degree Code</th>
								<th style="text-align:center">Type</th>
								<th style="text-align:center">Title</th>
								<th style="text-align:center">CIP Code</th>
								<th style="text-align:center">Faculty Proposer</th>
								<th style="text-align:center">Review Period</th>
								<th style="text-align:center">Concerns?</th>
								<th style="text-align:center">Status</th>
								<th style="text-align:center">Documents</th>
								<th style="text-align:center">Comments</th>
							</tr>
					</thead>
					<tbody>
						<?php if($proposals): ?>
							<?php foreach($proposals as $proposal): ?>
								<?php if($proposal->archive_flag): ?>
								<tr>
									<td style="text-align:center"><?php echo $proposal->get_coll_code(); ?></td>
									<td style="text-align:center"><?php echo $proposal->get_campus(); ?></td>
									<td style="text-align:center"><?php echo $proposal->degc_code; ?></td>
									<td style="text-align:center"><?php echo $proposal->type; ?></td>
									<td style="text-align:center"><?php echo $proposal->title; ?></td>
									<td style="text-align:center"><?php echo format_cip($proposal->get_cipc_code(), $proposal->get_cipc_track()); ?></td>
									<td style="text-align:center"><?php echo $proposal->faculty_proposer; ?></td>
									<td style="text-align:center"><?php echo $proposal->review_period; ?></td>
									<td style="text-align:center"><?php echo $proposal->concerns; ?></td>
									<td><ul style="margin-bottom:0;">
									</ul><?php if(!empty($proposal->status_collection)): ?>
										<ul style="margin-bottom:0;">
											<?php foreach($proposal->status_collection as $status): ?>
												<li><?php echo $status->status_text; ?></li>
											<?php endforeach; ?>
										</ul>
										<?php endif; ?></td>
									<td><ul style="margin-bottom:0;">
									</ul><?php if(!empty($proposal->document_collection)): ?>
											<ul style="margin-bottom:0;">
											<?php foreach($proposal->document_collection as $document): ?>
												<li><a href="<?php echo doc_url() . $document->filename; ?>" target="_blank"><?php echo $document->doc_name; ?></a></li>
											<?php endforeach; ?>
											</ul>
										<?php endif; ?></td>
										<td><ul style="margin-bottom:0;">
										</ul><?php if(!empty($proposal->comment_collection)): ?>
												<ul style="margin-bottom:0;">
												<?php foreach($proposal->comment_collection as $comment): ?>
													<li><?php echo $comment->comment_text; ?></li>
												<?php endforeach; ?>
												</ul>
											<?php endif; ?></td>
									<td style="min-width:100px;"><a href="<?php echo base_url(); ?>intent-proposal/edit?prop_id=<?php echo bin2hex($proposal->prop_id); ?>" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
											<button class="btn btn-danger del-int-prop" data-propid="<?php echo bin2hex($proposal->prop_id); ?>"><span class="glyphicon glyphicon-trash"></span></button>
									</td>
								</tr>
								<?php endif; ?>
							<?php endforeach; ?>
						<?php else: ?>
							<tr><td colspan="12" class="text-center">No proposals available.</td></tr>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php $this->load->view('_adminfooter'); ?>
