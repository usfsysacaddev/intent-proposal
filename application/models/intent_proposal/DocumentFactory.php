<?php
    /**
	 * Document Factory
	 *
	 * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */

	class DocumentFactory
	{
		/**
		 * Creates document object.
		 *
		 * @param array $params OPTIONAL document parameters
		 * @return object document object, false if none
		 * @access public
		 * @static
		 */
		public static function create_document($params = array())
		{
			$obj = false;
			if(class_exists('AdminDocumentModel'))
				$obj = new AdminDocumentModel($params);
			elseif(class_exists('DocumentModel'))
				$obj = new DocumentModel($params);

			return $obj;
		}
	}
?>
