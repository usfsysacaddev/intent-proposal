<?php
    /**
	 * AdminDocumentModel
	 *
	 * Document model admin functions
	 *
   * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */

    require_once('DocumentModel.php');
	class AdminDocumentModel extends DocumentModel
	{
		/**
		 * Handles user authentication, if not already done.
		 * Creates document object.
		 *
		 * @param array $params the parameters for this status object
		 * @access public
		 */
		public function __construct($params = array())
		{
			parent::__construct();

			//Check if the user has already been created
			if(!class_exists("UserModel"))
			{
				//Attempt to authenticate user
				$framework = get_instance();
				$framework->load->model('UserModel');
				if(!$this->UserModel->is_authorized)
				{
					//Log error and kill
					show_error('User not authenticated.', 403);
					log_message('error', 'User authentication failed: ' . $this->UserModel->username);
					die();
				}
			}

			$this->set_params($params);
		}

		/**
		 * Sets document parameters.
		 *
		 * @param array $params parameters to be set
		 * @access public
		 */
		public function set_params($params)
		{
			parent::set_params($params); //we're just loosening visibility
		}

		/**
		 * Saves document data to db
		 *
		 * @access public
		 */
		public function save()
		{
			//Create document id, if needed
			$this->create_id();

			//Create data array
			$data = array(
				'doc_id' => $this->doc_id,
				'prop_id' => $this->prop_id,
				'doc_name' => $this->doc_name,
        'order' => $this->order,
				'filename' => $this->filename,
				'modified_doc_date' => date('Y-m-d G:i:s'),
				'modified_doc_user' => $this->UserModel->username
				);

			//Commit to db
			$this->db->replace('intent_prop_documents', $data);

		}

		/**
		 * Deletes document from file system and db
		 *
		 * @return bool success flag
		 * @access public
		 */
		public function delete()
		{
			$success = unlink(doc_path() . $this->filename);

			if($success)
			{
				//Remove database record
				if($this->db->delete('intent_prop_documents', array('doc_id' => $this->doc_id)))
					$success = true;
			}

			return $success;
		}

		/**
		 * Create Document ID
		 *
		 * @access public
		 */
		public function create_id()
		{
			$this->doc_id = empty($this->doc_id) ? hex2bin(md5($this->prop_id . $this->doc_name)) : $this->doc_id;
		}
	}
?>
