<?php
    /**
	 * Proposal Collection (Admin)
	 *
	 * Administrative functionality for
	 * proposal collection.
	 *
   * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */
   
    require_once('ProposalCollectionModel.php');
	class AdminProposalCollectionModel extends ProposalCollectionModel
	{
		/**
		 * Handles user authentication, if not already done.
		 *
		 * @access public
		 */
		public function __construct()
		{
			parent::__construct();

			//Check if the user has already been created
			if(!class_exists("UserModel"))
			{
				//Attempt to authenticate user
				$framework = get_instance();
				$framework->load->model('UserModel');
				if(!$this->UserModel->is_authorized)
				{
					//Log error and kill
					show_error('User not authenticated.', 403);
					log_message('error', 'User authentication failed: ' . $this->UserModel->username);
					die();
				}
			}
		}
	}
?>
