<?php
    /**
	 * Comment
	 *
	 *
	 *
   * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */

    require_once(APPPATH . 'helpers/utils_helper.php');
	class CommentModel extends CI_Model
	{
		/**
	     * Comment ID
	     *
	     * @var string $comment_id
	     * @access public
	     */
		public $comment_id;

		/**
	     * Comment text
	     *
	     * @var string $comment_text
	     * @access public
	     */
		public $comment_text;

		/**
	     * Proposal ID
	     *
	     * @var string $prop_id
	     * @access public
	     */
		public $prop_id;

	    /**
       * Mpdified Date
       *
       * @var string $modified_comment_date
       * @access public
       */
    public $modified_comment_date;

    /**
       * Modified User
       *
       * @var string $modified_comment_user
       * @access public
       */
    public $modified_comment_user;

		/**
		 * Creates comment object.
		 *
		 * @param array $params OPTIONAL comment parameters
		 * @access public
		 */
		public function __construct($params = array())
		{
			$this->set_params($params);
		}

		/**
		 * Sets comment parameters.
		 *
		 * Not publically available until Admin
		 * @param array $params parameters to be set
		 * @access protected
		 */
		protected function set_params($params)
		{
			$this->comment_id = isset($params['comment_id']) ? get_binary($params['comment_id']) : $this->comment_id;
			$this->comment_text = isset($params['comment_text']) ? $params['comment_text'] : $this->comment_text;
			$this->prop_id = isset($params['prop_id']) ? get_binary($params['prop_id']) : $this->prop_id;
      $this->modified_comment_date = isset($params['modified_comment_date']) ? $params['modified_comment_date'] : $this->modified_comment_date;
      $this->modified_comment_user = isset($params['modified_comment_user']) ? $params['modified_comment_user'] : $this->modified_comment_user;
		}

		/**
		 * Sets comment parameters by comment id
		 *
		 * @param string $comment_id comment id
		 * @return bool success flag
		 * @access public
		 */
		public function set_params_by_id($comment_id)
		{
			//Attempt to fetch from db
			$this->db->select(array('comment_id', 'prop_id', 'comment_text','modified_comment_date', 'modified_comment_user'));
			$this->db->from('intent_prop_comments');
			$this->db->order_by('comment_text','ASC');
			$this->db->where('comment_id', get_binary($comment_id));

			$query = $this->db->get();

			//if result, set object params
			$success = false;
			if($query->row())
			{
				$this->set_params((array)$query->row());
				$success = true;
			}

			return $success;
		}
	}
?>
