$(document).ready(function () {
    //DataTables
    $('.datatable').each(function() {
        var table = $(this).dataTable( {
            paging:   false,
            ordering: true,
            order: [[ 0, "asc" ], [ 1, "asc" ], [ 2, "asc" ]],
            dom: 'Trpt'
        } );
        new $.fn.dataTable.FixedHeader( table );
    });
});