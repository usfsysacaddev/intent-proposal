<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Dashboard | USF Academic Planning</title>

<!-- Bootstrap -->
<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">

<!-- jQuery UI -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>css/sysintranet.min.css">

<?php if(ENVIRONMENT === 'development'): ?>
<!-- Less -->
<link rel="stylesheet/less" type="text/css" href="<?php echo base_url(); ?>css/sysintranet.less">
<?php endif; ?>

</head>
<body>
	<div class="top-bar">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6">
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="dropdown pull-right">
						<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<span class="glyphicon glyphicon-user"></span> <?php echo $this->UserModel->username; ?> <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<li><a href="//netid.usf.edu?logout=true">Logout</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="top-nav">
		<div class="container">
			<div class="row">
				<nav class="navbar navbar-default">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<style>
						.row {
						  display: table;
						  width: 100%;
						  table-layout: fixed;
						  margin-bottom: 10px;
						}
						.element-wrapper {
						  display: table-cell;
						  vertical-align: top;
						}
						.element {
						  width: 80%;
						  height: 30px;
							text-align: center;
						  margin: auto;
						}

						.button a {
							display: block;
					    width: 100%;
					    height: 100%;
							padding: 10px 10px;
						  font-size: 12px;
							margin-top: 5px;
						  cursor: pointer;
						  text-align: center;
						  text-decoration: none;
						  outline: none;
						  color: #fff;
						  background-color: #006747;
						  border: none;
						  border-radius: 15px;
						  /*box-shadow: 0 5px #999;*/
						}

						.button:hover {background-color: #e7e7e7}

						.button:active {
						  background-color: #e7e7e7;
						  box-shadow: 0 5px #666;
						  transform: translateY(4px);
						}
						</style>
						<div class="row">
							<div class="element-wrapper">
								<div class=" element">
								<?php if(strtolower($this->router->class) == 'proposal_collection' || strtolower($this->router->class) == 'proposal'): ?> class="active"
								<?php endif; ?>
								<p class="button"><a href="https://academicplanning.usf.edu/new-degree-proposals-tracking/admin" target="_blank">Degree Program Admin</a></p>
							</div>
						</div>
						<div class="element-wrapper">
							<div class=" element">
								<?php if($this->UserModel->username == "cynthiab" || $this->UserModel->username == "rajdesai" || $this->UserModel->username == "ryanborum"):?>
									<?php if(strtolower($this->router->class) == 'proposal_collection' || strtolower($this->router->class) == 'proposal'): ?> class="active"
									<?php endif; ?>
									<p class="button"><a href="<?php echo base_url();?>intent-proposal-collection/admin" target="_blank">Intent Proposals Admin</a></p>
								<?php endif;?>
							</div>
						</div>
						<div class="element-wrapper">
							<div class=" element">
								<p class="button"><a href="https://academicplanning.usf.edu/curriculum-codes" target="_blank">Curriculum Codes</a></p>
							</div>
					</div>
					</div>
				</div>
				</nav>
			</div>
		</div>
	</div>
