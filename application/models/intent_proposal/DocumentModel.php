<?php
    /**
	 * Document
	 *
	 * Document model.
	 *
   * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */

    require_once(APPPATH . 'helpers/utils_helper.php');
	class DocumentModel extends CI_Model
	{
		/**
	     * Document ID
	     *
	     * @var string $doc_id
	     * @access public
	     */
		public $doc_id;

		/**
	     * Document name
	     *
	     * @var string $doc_name
	     * @access public
	     */
		public $doc_name;

    /**
	     * Order
	     *
	     * @var string $order
	     * @access public
	     */
		public $order;

		/**
	     * Doc filename
	     *
	     * @var string $filename
	     * @access public
	     */
		public $filename;

		/**
	     * Proposal ID
	     *
	     * @var string $prop_id
	     * @access public
	     */
		public $prop_id;

		/**
	     * Mpdified Date
	     *
	     * @var string $modified_doc_date
	     * @access public
	     */
		public $modified_doc_date;

		/**
	     * Modified User
	     *
	     * @var string $modified_doc_user
	     * @access public
	     */
		public $modified_doc_user;

		/**
		 * Creates document object.
		 *
		 * @param array $params OPTIONAL document parameters
		 * @access public
		 */
		public function __construct($params = array())
		{
			$this->set_params($params);
		}

		/**
		 * Sets document parameters.
		 *
		 * Protected until Admin.
		 * @param array $params parameters to be set
		 * @access protected
		 */
		protected function set_params($params)
		{
			$this->doc_id = isset($params['doc_id']) ? get_binary($params['doc_id']) : $this->doc_id;
			$this->prop_id = isset($params['prop_id']) ? get_binary($params['prop_id']) : $this->prop_id;
			$this->doc_name = isset($params['doc_name']) ? $params['doc_name'] : $this->doc_name;
      $this->order = isset($params['order']) ? $params['order'] : $this->order;
			$this->filename = isset($params['filename']) ? $params['filename'] : $this->filename;
			$this->modified_doc_date = isset($params['modified_doc_date']) ? $params['modified_doc_date'] : $this->modified_doc_date;
			$this->modified_doc_user = isset($params['modified_doc_user']) ? $params['modified_doc_user'] : $this->modified_doc_user;
		}

		/**
		 * Sets document parameters by document id.
		 *
		 * @param string $doc_id document id to fetch from DB
		 * @return bool success flag
		 * @access public
		 */
		public function set_params_by_id($doc_id)
		{
			//Attempt to fetch from db
			$this->db->select('doc_id, prop_id, doc_name, order, filename, modified_doc_date, modified_doc_user');
			$this->db->from('intent_prop_documents');
			$this->db->where('doc_id', get_binary($doc_id));
			$query = $this->db->get();


			//if result, set object params
			$success = false;
			if($query->row())
			{
				$this->set_params((array)$query->row());
				$success = true;
			}

			return $success;
		}
	}
?>
