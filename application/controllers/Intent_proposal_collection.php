<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Intent_proposal_collection extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		require_once('D:\IIS\academicplanning$\wwwroot\guard.php');
	}

	/**
	 * Index page for this controller.
	 */
	public function index()
	{
		//Load proposal collection
		$this->load->model('intent_proposal/ProposalCollectionModel', 'ProposalCollectionModel');
		$this->ProposalCollectionModel->populate();

		//Load view
		$this->load->helper(array('output','utils'));
		$this->load->view('intent_proposal/all_proposals', array('proposals' => $this->ProposalCollectionModel->proposal_collection));
	}

	/**
	 * Archived proposals.
	 */
	public function archive()
	{
		//Load proposal collection
		$this->load->model('intent_proposal/ProposalCollectionModel', 'ProposalCollectionModel');
		$this->ProposalCollectionModel->populate();

		//Load view
		$this->load->helper(array('output','utils'));
		$this->load->view('intent_proposal/proposal_archive', array('proposals' => $this->ProposalCollectionModel->proposal_collection));
	}

	/**
	 * Admin page for this controller.
	 */
	public function admin()
	{
		//Check that user is authorized to be here
		$this->load->model('UserModel');
		if(!$this->UserModel->is_authorized)
		{
			show_error('User not authenticated.', 403);
			log_message('error', 'User authentication failed: ' . $this->UserModel->username);
			die();
		}

		//Load proposal collection
		$this->load->model('intent_proposal/AdminProposalCollectionModel', 'AdminProposalCollectionModel');
		$this->AdminProposalCollectionModel->populate($this->UserModel->campus); //specific to user's institution

		//Load view
		$this->load->helper(array('output','utils'));
		$this->load->view('intent_proposal/admin_all_proposals', array('proposals' => $this->AdminProposalCollectionModel->proposal_collection));
	}
}
