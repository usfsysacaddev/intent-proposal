<?php
    /**
	 * Status
	 *
	 * Proposal status
	 *
   * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */

    require_once(APPPATH . 'helpers/utils_helper.php');
	class StatusModel extends CI_Model
	{

		/**
	     * Status ID
	     *
	     * @var string $status_id
	     * @access public
	     */
		public $status_id;

		/**
	     * Status text
	     *
	     * @var string $status_text
	     * @access public
	     */
		public $status_text;

    /**
       * Order
       *
       * @var string $order
       * @access public
       */
    public $order;

		/**
	     * Proposal ID
	     *
	     * @var string $prop_id
	     * @access public
	     */
		public $prop_id;

    /**
       * Modified Date
       *
       * @var string $modified_status_date
       * @access public
       */
    public $modified_status_date;

    /**
       * Modified User
       *
       * @var string $modified_status_user
       * @access public
       */
    public $modified_status_user;

		/**
		 * Creates status object.
		 *
		 * @param array $params OPTIONAL status parameters
		 * @access public
		 */
		public function __construct($params = array())
		{
			$this->set_params($params);
		}

		/**
		 * Sets status parameters.
		 *
		 * Not publically available until Admin
		 * @param array $params parameters to be set
		 * @access protected
		 */
		protected function set_params($params)
		{
			$this->status_id = isset($params['status_id']) ? get_binary($params['status_id']) : $this->status_id;
			$this->status_text = isset($params['status_text']) ? $params['status_text'] : $this->status_text;
      $this->order = isset($params['order']) ? $params['order'] : $this->order;
			$this->prop_id = isset($params['prop_id']) ? get_binary($params['prop_id']) : $this->prop_id;
      $this->modified_status_date = isset($params['modified_status_date']) ? $params['modified_status_date'] : $this->modified_status_date;
      $this->modified_status_user = isset($params['modified_status_user']) ? $params['modified_status_user'] : $this->modified_status_user;
		}

		/**
		 * Sets status parameters by status id
		 *
		 * @param string $status_id status id
		 * @return bool success flag
		 * @access public
		 */
		public function set_params_by_id($status_id)
		{
			//Attempt to fetch from db
			$this->db->select(array('status_id', 'prop_id', 'status_text', 'order','modified_status_date', 'modified_status_user'));
			$this->db->from('intent_prop_status');
			$this->db->order_by('status_text','ASC');
			$this->db->where('status_id', get_binary($status_id));

			$query = $this->db->get();

			//if result, set object params
			$success = false;
			if($query->row())
			{
				$this->set_params((array)$query->row());
				$success = true;
			}

			return $success;
		}
	}
?>
