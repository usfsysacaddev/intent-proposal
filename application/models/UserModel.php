<?php
    /**
	 * User
	 *
	 * Handles application user.
	 *
	 * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */

	class UserModel extends CI_Model
	{

		/**
	     * User's username
	     *
	     * Formatted all lowercase.
	     *
	     * @var string $username
	     * @access public
	     */
		public $username;

		/**
	     * User's email
	     *
	     * Formatted all lowercase.
	     *
	     * @var string $email
	     * @access public
	     */
		public $email;

		/**
	     * User's Name
	     *
	     * @var array $name array containing 'First' and 'Last' names
	     * @access public
	     */
		public $name;

    public $attributes = [];

		/**
	     * User's USF Institution Code
	     *
	     * @var string $campus USF institution: USF, USFSP, USFSM, USFH
	     * @access public
	     */
		public $campus;

		/**
	     * Is authorized flag
	     *
	     * @var bool $is_authorized Flag indicating user's auth state
	     * @access public
	     */
		public $is_authorized;


		/**
		 * Creates user object if auto
		 * create is set.
		 *
		 * @param array $params available opts include: 'autoCreate' (bool) - when set to true, automatically creates user object (default)
		 * @access public
		 */
		public function __construct($params = array())
		{
			$this->load->database();

			$defaults = array(
				'autoCreate' => true
				);
			$params = array_merge($defaults, $params); //extend defaults with params

			if($params['autoCreate']) //kick off user creation
				$this->setUser();
		}

		/**
		 * Sets user parameters.
		 *
		 * @access public
		 */
		public function setUser()
		{
			require_once(AUTH_LIB);
			//Authenticate through USF ALREADY DONE
			$params = array('autoCreate' => true, 'environment' => 'production');
			$usfUser = new USFUser($params);
			$this->username = strtolower($usfUser->netid);
            $this->email = strtolower($usfUser->email);
            $this->attributes = $usfUser->attributes;

			//Attempt to pull user from DB
			$sql = 'SELECT netid, firstname, lastname, institution FROM users WHERE netid = LOWER(?);';
			$params = array(strtolower($usfUser->netid));
			$query = $this->db->query($sql, $params);

            //Set User
            if(!empty($query->row()))
            {
            	$this->name = array('First' => $query->row()->firstname, 'Last' => $query->row()->lastname);
            	$this->campus = strtoupper($query->row()->institution);
            	$this->is_authorized = true;
            }
		}

		/**
		 * Determines if user can edit institution
		 *
		 * @param string $campus Institution code in question
		 * @return bool whether or not user can edit provided institution
		 * @access public
		 */
		public function can_edit($campus)
		{
			return (strtoupper($campus) == strtoupper($this->campus) || strtoupper($this->campus) == 'SYS') ? true : false;
		}

		/**
		 * Determines if user can delete
		 *
		 * @param string $campus Institution code in question
		 * @return bool whether or not user can delete within provided institution
		 * @access public
		 */
		public function can_delete($campus)
		{
			return (strtoupper($this->campus) == 'SYS') ? true : false;
		}

		/**
		 * Added to hide certain "progressive" features
		 * from stubborn users who refuse to use them.
		 *
		 * @return bool whether or not user is stubborn
		 * @access public
		 */
		public function is_stubborn()
		{
			$stubborn_netids = array();
			return (in_array(strtolower($this->username), $stubborn_netids)) ? true : false;
		}
	}
?>
