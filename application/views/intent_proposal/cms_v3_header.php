<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><?php echo $header['title'];?>Academic Planning | ODS | USF</title> <!-- TODO: -->

  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Barlow+Condensed:400,600" rel="stylesheet">
  <!--<link type="text/css" rel="stylesheet" href="https://www.usf.edu/_resources/css/v3/global.css?ver=4.5" />-->
  <link type="text/css" rel="stylesheet" href="<?php echo $base_url; ?>intent-proposal/css/global.css?ver=4.5" />
  <link type="text/css" rel="stylesheet" href="https://www.usf.edu/_resources/css/v3/secondary.css?ver=5.2" />
  <link type="text/css" rel="stylesheet" media="print" href="https://www.usf.edu/_resources/css/v3/print.css?ver=0.1" />
  <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://www.usf.edu/_resources/js/v3/functions.js?ver=4"></script>

  <!-- Added by Academic Planning -->
  <link type="text/css" rel="stylesheet" media="screen" href="<?php echo $base_url; ?>css/addtl.min.css" />
  <link type="text/css" rel="stylesheet" media="screen" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.css"/>
  <link type="text/css" rel="stylesheet" media="screen" href="<?php echo $base_url; ?>curriculum-codes/css/dataTables.fixedHeader.min.css"/>
  <link type="text/css" rel="stylesheet" media="screen" href="<?php echo $base_url; ?>curriculum-codes/css/dataTables.fixedColumns.min.css"/>
  <link type="text/css" rel="stylesheet" media="screen" href="<?php echo $base_url; ?>curriculum-codes/css/dataTables.tableTools.min.css"/>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://academicplanning.usf.edu/intent-proposal/js/submitted-proposals.js?ver=4"></script>

  <meta name="Author" content="University of South Florida">


  <style>
  .header--alt .donate_icon, .header--unit .donate_icon {
      color: #9CCB3B;
    }
  </style>
</head>
<?php include_once("analyticstracking.php") ?>

<body><a class="skipLink" href="#content">Skip to Main Content</a>
  <header class="header--alt global">
    <div class="header_banner u-clearfix">

      <p class="header_text"><a href="https://www.usf.edu">University of South Florida</a></p>
      <h2 class="header_title"><a class="header_titleLink" href="/"><?php echo $header['header_title']; ?></a></h2>
    </div>
    <div class="header_search">
      <p class="search_toggle toggle toggle--global">
        <span class="search_icon u-icon">
          <span class="hidden">Search</span>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" aria-hidden="true">
            <path fill="currentColor"
              d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z" />
            </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" aria-hidden="true">
            <path fill="currentColor"
              d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z">
            </path>
          </svg>
        </span>
      </p>
      <div class="search_content">
        <form class="search_form" method="get" action="//usfweb.usf.edu/search/#" role="search">
          <label class="hidden" for="site-search-input">Search for: </label>
          <input id="site-search-input" type="text" name="cludoquery" size="20" maxlength="256" value="" />
          <input id="site-search-submit" type="image" src="https://www.usf.edu/_resources/images/v3/global/png/search.png" alt="Go" />
          <input type="hidden" name="cludopage" value="1" />
          <input type="hidden" name="2920E24" value="986B2E94EAC60BB1F95E96DF26EE0F5F"></form>
      </div>
    </div>

    <div class="header_nav">
      <h2 class="hidden">Main Navigation</h2>
      <p class="nav_toggle toggle toggle--global"><span class="nav_icon u-icon"><span class="hidden">Menu</span>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" aria-hidden="true">
            <path fill="currentColor"
              d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z" />
            </svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" aria-hidden="true">
            <path fill="currentColor"
              d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z">
            </path>
          </svg></span></p>
      <div class="nav_content">
        <div class="donate">
          <a class="donate_link" href="https://giving.usf.edu/how/herdfunder" target="_blank" onclick="ga('send', 'event', 'cta', 'click', 'Bulls United in Action'); ga('account2.send', 'event', 'cta', 'click', 'Bulls United in Action');"><span
              class="donate_icon u-icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" aria-hidden="true">
                <path fill="currentColor"
                  d="M464 160h-39.3c9.5-13.4 15.3-29.9 15.3-48 0-44.1-33.4-80-74.5-80-42.3 0-66.8 25.4-109.5 95.8C213.3 57.4 188.8 32 146.5 32 105.4 32 72 67.9 72 112c0 18.1 5.8 34.6 15.3 48H48c-26.5 0-48 21.5-48 48v96c0 8.8 7.2 16 16 16h16v112c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V320h16c8.8 0 16-7.2 16-16v-96c0-26.5-21.5-48-48-48zm-187.8-3.6c49.5-83.3 66-92.4 89.3-92.4 23.4 0 42.5 21.5 42.5 48s-19.1 48-42.5 48H274l2.2-3.6zM146.5 64c23.4 0 39.8 9.1 89.3 92.4l2.1 3.6h-91.5c-23.4 0-42.5-21.5-42.5-48 .1-26.5 19.2-48 42.6-48zM192 448H80c-8.8 0-16-7.2-16-16V320h128v128zm0-160H32v-80c0-8.8 7.2-16 16-16h144v96zm96 160h-64V192h64v256zm160-16c0 8.8-7.2 16-16 16H320V320h128v112zm32-144H320v-96h144c8.8 0 16 7.2 16 16v80z" />
                </svg></span>

            <div class="donate_text">
              <p class="donate_action">Contribute to our future</p>
            </div>
          </a>
        </div>
        <nav class="siteNav">
          <ul class="siteNav_menu u-list">
            <li class="siteNav_item"><a class="siteNav_link" href="/curriculum/academic-catalogs.php" target="_blank">Academic Catalogs</a></li>
            <li class="siteNav_item"><a class="siteNav_link" href="/proposals/tracking/courses" target="_blank">Course Proposals</a></li>
            <li class="siteNav_item"><a class="siteNav_link" href="/" target="_blank">Academic Planning</a></li>
            <li class="siteNav_item"><a class="siteNav_link" href="/curriculum-codes" target="_blank">Curriculum Codes</a></li>
            <li class="siteNav_item"><a class="siteNav_link" href="/integrated-curricula" target="_blank">Integrated Curricula</a></li>
            <li class="siteNav_item"><a class="siteNav_link" href="/degree-inventory.php" target="_blank">Degree Inventories</a></li>
          </ul>
        </nav>
        <div class="auxNav">
          <div class="auxNav_content u-clearfix">

            <nav class="utilNav">
              <ul class="utilNav_menu u-list u-clearfix">
                <li class="utilNav_item"><a class="utilNav_link" href="https://my.usf.edu/">MyUSF</a></li>
                <li class="utilNav_item"><a class="utilNav_link" href="http://directory.usf.edu/">Directory</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>
