<?php
    /**
	 * AdminProposalModel
	 *
	 * New program proposal (admin)
	 *
   * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */
    require_once('ProposalModel.php');
    require_once('AdminDocumentModel.php');
    require_once('AdminStatusModel.php');
    require_once('AdminCommentModel.php');

	class AdminProposalModel extends ProposalModel
	{

		/**
		 * Perform authentication check.
		 * Creates document object.
		 *
		 * @param array $params OPTIONAL document parameters
		 * @access public
		 */
		public function __construct($params = array())
		{
			parent::__construct();

			//Check if the user has already been created
		if(!class_exists("UserModel"))
			{
				//Attempt to authenticate user
				$framework = get_instance();
				$framework->load->model('UserModel');
				if(!$framework->UserModel->is_authorized)
				{
					//Log error and kill
					show_error('User not authenticated.', 403);
					log_message('error', 'User authentication failed: ' . $framework->UserModel->username);
					die();
				}
			}
			$this->set_params($params);
		}

		/**
		 * Sets proposal parameters.
		 *
		 * @param array $params parameters to be set
		 * @access public
		 */
		public function set_params($params)
		{
			parent::set_params($params); //we're just loosening visibility
		}

		/**
		 * Saves proposal to db
		 *
		 * @access public
		 */
		public function save()
		{
			//Create proposal id, if needed
			$this->create_id();

			//Create data array
			$data = array(
				'prop_id' => $this->prop_id,
				'campus' => $this->_campus,
				'coll_code' => $this->_coll_code,
        'type' => $this->type,
				'degc_code' => $this->degc_code,
				'title' => $this->title,
				'cipc_code' => $this->_cipc_code,
        'review_period' => $this->review_period,
        'concerns' => $this->concerns,
				'cipc_track' => $this->_cipc_track,
				'faculty_proposer' => $this->faculty_proposer,
				'archive_flag' => $this->archive_flag,
				'modified_date' => date('Y-m-d G:i:s'),
				'modified_user' => $this->UserModel->username
				);

			//Commit to db
			$this->db->replace('intent_proposals', $data);

			//Update all docs, comments & statuses

      // Saves to DB
			foreach((array)$this->document_collection as $doc)
			{
				$doc->save();
			}

			foreach((array)$this->status_collection as $status)
			{
				$status->save();
			}

      foreach((array)$this->comment_collection as $comment)
      {
        $comment->save();
      }
		}

		/**
		 * Deletes proposal
		 *
		 * @return bool success flag
		 * @access public
		 */
		public function delete()
		{
			$success = false;

			//Physically delete documents
			if($this->document_collection)
			{
				foreach($this->document_collection as $document)
				{
					$document->delete();
				}
			}

			//Remove database record
			if($this->db->delete('intent_proposals', array('prop_id' => $this->prop_id)))
				$success = true;

			return $success;
		}

		/**
		 * Creates proposal id
		 *
		 * @access public
		 */
		public function create_id()
		{
			$this->prop_id = empty($this->prop_id) ? hex2bin(md5($this->_campus . $this->_coll_code . $this->degc_code . $this->title . date('Y-m-d'))) : $this->prop_id;
		}

		/**
		 * Removes document from collection
		 *
		 * @param string $doc_id The doc id of the document to be removed
		 * @access public
		 */
		public function remove_from_document_collection($doc_id)
		{
			$doc_id = bin2hex(get_binary($doc_id)); //make sure we have the hexxed... weird.

			if(isset($this->document_collection[$doc_id]))
				unset($this->document_collection[$doc_id]);
		}

		/**
		 * Removes status from collection
		 *
		 * @param string $status_id The status id to be removed
		 * @access public
		 */
		public function remove_from_status_collection($status_id)
		{
			$status_id = bin2hex(get_binary($status_id)); //make sure we have the hexxed... weird.

			if(isset($this->status_collection[$status_id]))
				unset($this->status_collection[$status_id]);
		}

    /**
     * Removes comment from collection
     *
     * @param string $comment_id The comment id to be removed
     * @access public
     */
    public function remove_from_comment_collection($comment_id)
    {
      $comment_id = bin2hex(get_binary($comment_id)); //make sure we have the hexxed... weird.

      if(isset($this->comment_collection[$comment_id]))
        unset($this->comment_collection[$comment_id]);
    }
	}
?>
