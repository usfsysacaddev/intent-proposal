<?php
    /**
	 * Status Factory
	 *
	 * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */

	class StatusFactory
	{
		/**
		 * Creates status object.
		 *
		 * @param array $params OPTIONAL status parameters
		 * @return object status object, false if none
		 * @access public
		 * @static
		 */
		public static function create_status($params = array())
		{
			$obj = false;
			if(class_exists('AdminStatusModel'))
				$obj = new AdminStatusModel($params);
			elseif(class_exists('StatusModel'))
				$obj = new StatusModel($params);

			return $obj;
		}
	}
?>
