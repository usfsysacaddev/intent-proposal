<!-- Archived Intent Proposals -->
<?php

	$header['title'] = "Archived Intent Proposals | ";
	$header['description'] = "USF Academic Planning Archived Intent Proposals";
	$header['keywords'] = "";
	$header['header_title'] = "Archived Intent Proposals";
?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('../includes/vars.php');
$styles = array('//cdn.datatables.net/1.10.7/css/jquery.dataTables.css', base_url() . 'css/dataTables.fixedHeader.min.css');
require_once('cms_v3_header.php');
?>

<style>
.row {
  display: table;
  width: 100%;
  table-layout: fixed;
  margin-bottom: 10px;
}
.element-wrapper {
  display: table-cell;
  vertical-align: top;
}
.element {
  width: 80%;
  height: 30px;
	text-align: center;
  margin: auto;
}

.button {
  display: inline-block;
  padding: 10px 20px;
  font-size: 16px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #EFEFF0;
  border: none;
  border-radius: 15px;
  box-shadow: 0 5px #999;
}

.button:hover {background-color: #EFEFF0}

.button:active {
  background-color: #EFEFF0;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}
</style>

	<div id="content" class="container" role="main" style="margin-right: 25px; margin-left:25px;">
    	<!--<h1 class="page-title-plain-text">Archived Intent Proposals</h1>-->
			<br>
			<nav class="breadcrumbs" style="margin-left: 840px;">
	      <a href="https://www.usf.edu/ods/" target="_blank">ODS</a> &gt; <a href="<?php echo $base_url; ?>" target="_blank">Academic Planning</a> &gt; <a href="https://academicplanning.usf.edu/curriculum/intent-proposal.php" target="_blank">Intent Proposal Requests</a> &gt; Archived Intent Proposals
	    </nav>
<br><br>
		<div class="row">
			<div class="element-wrapper">
				<div class="element">
					<button class="button"><a href="https://academicplanning.usf.edu/intent-proposal.php" target="_blank">View Intent Proposal Webpage</a></button>
					</div>
			</div>
		  <div class="element-wrapper">
		    <div class="element">
					<button class="button"><a href="<?php echo base_url(); ?>intent-proposal-collection" target="_blank">View Current Intent Proposals</a></button>
					</div>
		  </div>
		  <div class="element-wrapper">
		    <div class="element">
					<button class="button"><a href="https://academicplanning.usf.edu/new-degree-proposals-tracking/" target="_blank">View Pre-Proposals/New Degree Proposals</a></button>
					</div>
		  </div>
	</div>
<br>
<br />
			<table class="table table-striped datatable">
				<thead>
					<tr>
							<th style="text-align:center">College</th>
							<th style="text-align:center">Campus</th>
							<th style="text-align:center">Degree Code</th>
							<th style="text-align:center">Type</th>
							<th style="text-align:center">Title</th>
							<th style="text-align:center">CIP Code</th>
							<th style="text-align:center">Faculty Proposer</th>
							<th style="text-align:center">Review Period</th>
							<th style="text-align:center">Concerns?</th>
							<th style="text-align:center">Status</th>
							<th style="text-align:center">Documents</th>
							<th style="text-align:center">Comments</th>
						</tr>
				</thead>
				<tbody>
					<?php if($proposals): ?>
						<?php foreach($proposals as $proposal): ?>
							<?php if($proposal->archive_flag): ?>
							<tr>
								<td style="text-align:center"><?php echo $proposal->get_coll_code(); ?></td>
								<td style="text-align:center"><?php echo $proposal->get_campus(); ?></td>
								<td style="text-align:center"><?php echo $proposal->degc_code; ?></td>
								<td style="text-align:center"><?php echo $proposal->type; ?></td>
								<td style="text-align:center"><?php echo $proposal->title; ?></td>
								<td style="text-align:center"><?php echo format_cip($proposal->get_cipc_code(), $proposal->get_cipc_track()); ?></td>
								<td style="text-align:center"><?php echo $proposal->faculty_proposer; ?></td>
								<td style="text-align:center"><?php echo $proposal->review_period; ?></td>
								<td style="text-align:center"><?php echo $proposal->concerns; ?></td>
								<!--Status Column -->
								<td>
								<?php if(!empty($proposal->status_collection)): ?>
										<?php foreach($proposal->status_collection as $status): ?>
											<li><?php echo $status->status_text; ?></li>
										<?php endforeach; ?>
									<?php endif; ?></td>
  							<!--Documents Column -->
								<td>
								<?php if(!empty($proposal->document_collection)): ?>
										<?php foreach($proposal->document_collection as $document): ?>
											<li><a href="<?php echo doc_url() . $document->filename; ?>" target="_blank"><?php echo $document->doc_name; ?></a></li>
										<?php endforeach; ?>
									<?php endif; ?></td>
								<!--Comments Column -->
								<td>
								<?php if(!empty($proposal->comment_collection)): ?>
										<?php foreach($proposal->comment_collection as $comment): ?>
											<li><?php echo $comment->comment_text; ?></li>
										<?php endforeach; ?>
								<?php endif; ?></td>
							</tr>
							<?php endif; ?>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
<?php $direct_edit = base_url() . 'concept-proposal-collection/admin';

$scripts = array('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js', base_url() . 'js/dataTables.fixedHeader.min.js', base_url() . 'js/submitted-proposals.js');

require_once('cms_v3_footer.php'); ?>
