<?php
    /**
	 * Proposal Collection
	 *
	 * A collection of proposal objects
	 *
   * @category	New Intent Proposal
	 * @package		USF Academic Planning
	 * @author 		Raj Desai <rajdesai@usf.edu>
	 * @copyright	2020 University of South Florida
	 * @version 	1
	 */
    require_once('ProposalModel.php');
	class ProposalCollectionModel extends CI_Model
	{
		/**
	     * Proposal collection
	     *
	     * @var array $proposal_collection Array of proposal objects
	     * @access public
	     */
		public $proposal_collection;

		/**
		 * Fetches all proposals
		 *
		 * @param string $campus Institution code to populate
		 * @access public
		 */

		public function populate($campus = false)
		{
			//Attempt to fetch from db
			$this->load->database();
			$this->db->select('intent_proposals.prop_id, campus, coll_code, type, degc_code,
                         title, cipc_code, review_period, concerns, cipc_track, faculty_proposer, archive_flag,
                         GROUP_CONCAT(DISTINCT HEX(doc_id) ORDER BY intent_prop_documents.order SEPARATOR "|") AS documents,
                         GROUP_CONCAT(DISTINCT HEX(status_id) ORDER BY intent_prop_status.order SEPARATOR "|") AS statuses,
                         GROUP_CONCAT(DISTINCT HEX(comment_id) ORDER BY comment_text SEPARATOR "|") AS comments', false);
			$this->db->from('intent_proposals');
			$this->db->join('intent_prop_documents', 'intent_proposals.prop_id = intent_prop_documents.prop_id', 'left');
			$this->db->join('intent_prop_status', 'intent_proposals.prop_id = intent_prop_status.prop_id', 'left');
      $this->db->join('intent_prop_comments', 'intent_proposals.prop_id = intent_prop_comments.prop_id', 'left');
			$this->db->group_by('prop_id');
			$this->db->order_by('campus ASC, coll_code ASC, title ASC');

			if($campus && $campus !== 'SYS')
				$this->db->where('campus', $campus);

			$query = $this->db->get();

			//if result, set object params
			if($query->result())
			{
				foreach($query->result() as $data)
				{
					$data->documents = empty($data->documents) ? null : explode('|', $data->documents);
					$data->statuses = empty($data->statuses) ? null : explode('|', $data->statuses);
          $data->comments = empty($data->comments) ? null : explode('|', $data->comments);

					$this->proposal_collection[] = new ProposalModel($data);
				}
			}
		}
	}
?>
