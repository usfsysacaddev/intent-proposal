<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * Formats CIP for export
	 *
	 * @param string $cipc_code The CIP Code
	 * @param string $cipc_track OPTIONAL the CIP track
	 */
	function format_cip($cipc_code, $cipc_track = false)
	{
		$formatted_cip = $cipc_code;
		if($cipc_track)
			$formatted_cip .= ' (Track ' . $cipc_track . ')';

		return $formatted_cip;
	}

	/**
	 * Splits CIP track in two
	 *
	 * @param string $cipc_track The CIP track
	 * @param int $cipc_track_part The part of the track to be returned (either 1 for 1st or 2 for 2nd)
	 * @return string cipc_track_part
	 */
	function get_cipc_track_part($cipc_track, $part)
	{
		$r = "";
		if($part >= 1 && $part <= 2 && $cipc_track)
		{
			preg_match_all("/([0-9][0-9]?) of ([0-9][0-9]?)/", $cipc_track, $cipc_track_parts);
			if(sizeof($cipc_track_parts) > 1)
				$r = $cipc_track_parts[$part][0];
		}
		return $r;
	}
